package ar.com.ciu.persistencia.tp1.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * Statement
 * 
 */
public class Main {

	private static Connection dbConnection = null;

	public static void main(String[] args) {
		try {
			//creo la conexion
			dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tpventas", "root", "admin");
			//Lo uso para crear sentencias sql
			Statement statement = dbConnection.createStatement();
	
			// insert
			String sql = "INSERT INTO cliente (codigo, apellido, nombre) VALUES ('D84','Jon','Stak')";
			statement.executeUpdate(sql);
				
			
			//termina de ejecutar la query
			statement.close();
			//cierro la conexion para liberar los recursos
			dbConnection.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		
		}
	}

}