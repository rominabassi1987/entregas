package ar.com.ciu.persistencia.tp1.model;

import java.util.Date;


public class Precio {

	private Integer id_precio;
	private Double monto; 
	private Date fecha;
	private Producto producto;
	private int id_producto;
	
	public Precio(Integer id_precio, Double monto, Date fecha, int i) {
		super();
		this.id_precio = id_precio;
		this.monto = monto;
		this.fecha = fecha;
		this.id_producto = i;
	}
	
	public Precio(Double monto, Date fecha, int i) {
		this.monto = monto;
		this.fecha = fecha;
		this.id_producto=   i;
		
	}

	public Integer getIdPrecio() {
		return id_precio;
	}
	public void setIdPrecio(Integer id) {
		this.id_precio = id;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public int getId_producto() {
		return id_producto;
	}

	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	
	



}