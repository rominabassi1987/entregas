package ar.com.ciu.persistencia.tp1.model;


public class Producto {

	private Integer id_producto;
	private String codigo; 
	private String descripcion;
	private Precio id_precio;
	
	public Producto(Integer id_producto, String codigo, String descripcion, Precio id_precio) {
		super();
		this.id_producto = id_producto;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.id_precio = id_precio;
	}
	
	public Integer getIdProducto() {
		return id_producto;
	}
	public void setIdProducto(Integer id) {
		this.id_producto = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Precio getIdPrecio() {
		return id_precio;
	}
	public void setCodigoDeArea(Precio id_precio) {
		this.id_precio = id_precio;
	}
	

}
