package ar.com.ciu.persistencia.tp1.model;

public class Cliente {

		// atributos
	//Para autoincrement: si esta definido en la DB no hace falta definir nada acá
	private Integer id_cliente;
	private String codigo;
	private String apellido;
	private String nombre;
	
		// constructor
	public Cliente(String codigo, String nombre, String apellido) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
	}

		// metodos

		// gets y sets
	public Integer getId() {
		return id_cliente;
	}
	
	public void setId(Integer id_cliente) {
		this.id_cliente = id_cliente;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getCodigo() {
		return codigo;
	}

	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}