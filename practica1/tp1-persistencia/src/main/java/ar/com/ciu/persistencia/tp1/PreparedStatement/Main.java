/*13. Mediante PreparedStatement cambiar el precio a un producto, esto implica crear un
precio nuevo (sin eliminar el que estaba de manera de dejar un historial) y actualizar el
producto para que apunte al nuevo precio.*/
package ar.com.ciu.persistencia.tp1.PreparedStatement;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import ar.com.ciu.persistencia.tp1.model.Precio;

/**
 * 
 * PreparedStatement transaction
 * 
 *
 */
public class Main {

	private static Connection dbConnection = null;

	public static void main(String[] args) throws ClassNotFoundException {
		try {

			dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tpventas", "root", "admin");
			dbConnection.setAutoCommit(false);
			
			Precio precio = new Precio(100, 425.30, new Date(), 1);
			

			String sqlPrecio = "insert into precio (id, monto, fecha, id_producto) VALUES (?, ?, ?, ?)";
	        PreparedStatement preparedStatement;
	        preparedStatement = dbConnection.prepareStatement(sqlPrecio);
	        preparedStatement.setInt(1, precio.getIdPrecio());

	        preparedStatement.setDouble(2, precio.getMonto());
	        preparedStatement.setDate(3, new java.sql.Date(precio.getFecha().getTime()));
	        preparedStatement.setInt(4, precio.getId_producto());

	        preparedStatement.executeUpdate();
	        preparedStatement.close();
	        dbConnection.close();
	        dbConnection.commit();
	        
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}		
	}

}
