USE tpventas;
/*1. Generar las tablas con sus correspondientes constraints, los códigos y el número de
factura no se pueden repetir.*/
CREATE TABLE cliente (
	id_cliente INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    codigo VARCHAR(16) NOT NULL,
    apellido VARCHAR(256) NOT NULL,
    nombre VARCHAR(256) NOT NULL
);

CREATE TABLE factura (
	id_factura INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_cliente INT NOT NULL,
    fecha DATETIME NOT NULL,
    numero INT NOT NULL,
FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente)
);

CREATE TABLE producto (
	id_producto INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    codigo VARCHAR(16) NOT NULL,
	descripcion VARCHAR(256) NOT NULL,
    id_precio INT NOT NULL
);

CREATE TABLE precio (
	id_precio INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    monto DECIMAL(8,2) NOT NULL,
	fecha DATE NOT NULL,
    id_producto INT NOT NULL,
FOREIGN KEY (id_producto) REFERENCES producto(id_producto)
);

ALTER TABLE producto
	ADD FOREIGN KEY (id_precio) REFERENCES precio(id_precio);

CREATE TABLE factura_producto (
	id_factura INT NOT NULL,
    id_producto INT NOT NULL,
    cantidad INT NOT NULL,
FOREIGN KEY (id_factura) REFERENCES factura(id_factura),
FOREIGN KEY (id_producto) REFERENCES producto(id_producto)
);


/*2. Generar un conjunto de datos de prueba.*/
INSERT INTO cliente(codigo, apellido, nombre)
	VALUES (004, 'Snow', 'Jhon'),
			(020, 'Stark', 'Arya'),
            (084, 'Lannister', 'Cercei');

INSERT INTO factura(id_cliente, fecha, numero)
	VALUES(1, '2021/07/18 10:05:05', 55),
			(2, '2022/10/25 08:55:00', 85),
			(3, '2020/09/03 09:14:25', 67);

INSERT INTO producto(codigo, descripcion)
	VALUES('pr08', 'Coca Cola 1,5L'),
			('pr244', 'Fanta 1,5L'),
            ('pr63', 'Sprite 1,5L'),
			('pr102', 'Aquarius 1,5L');
		
INSERT INTO factura_producto(id_factura, id_producto, cantidad)
	VALUES(3, 2, 6),
			(1, 3, 16),
            (1, 1, 24),
			(2, 3, 15);
            
INSERT INTO precio
	VALUES(22, 240.25, '2021/05/03 09:14:25', 4),
			(24, 230.50, '2018/03/07 10:17:25', 3),
            (41, 215.32, '2017/09/24 13:25:25', 2),
			(75, 290.10, '2015/07/10 10:10:25', 1);
            
INSERT INTO factura_producto(id_factura, id_producto, cantidad)
	VALUES(3, 5, 6),
			(1, 5, 16),
            (1, 6, 24),
			(2, 7, 15);


INSERT INTO producto(codigo, descripcion, id_precio)
	VALUES('pr08', 'Coca Cola 1,5L', 75),
			('pr244', 'Fanta 1,5L', 41),
            ('pr63', 'Sprite 1,5L', 24),
			('pr102', 'Aquarius 1,5L', 22);

INSERT INTO factura(id_cliente, fecha, numero)
	VALUES(1, '2020/05/03 09:14:25', 77),
			(1, '2014/03/07 10:17:25', 84),
            (2, '2018/09/24 13:25:25', 25),
			(1, '2016/07/10 10:10:25', 11);

/*3. Realizar una consulta que retorne: código, apellido y nombre del cliente, el número y
la fecha de sus facturas.*/
SELECT cliente.codigo, cliente.nombre, cliente.apellido, factura.numero, factura.fecha
FROM cliente
	INNER JOIN factura
	ON cliente.id_cliente = factura.id_cliente;

/*4. Realizar una consulta que detalle el historial de precios de un producto en particular
identificado por código.*/
SELECT precio.monto, precio.fecha, producto.descripcion
FROM precio
	INNER JOIN producto
	ON precio.id_precio = producto.id_precio
WHERE producto.id_producto = 5;

/*5. Realizar una consulta que retorne la cantidad total de ventas de cada producto
ordenado de mayor a menor.*/
SELECT factura_producto.cantidad, producto.descripcion
FROM factura_producto
	INNER JOIN producto
	ON factura_producto.id_producto = producto.id_producto
ORDER BY cantidad DESC;

/*6. Realizar una consulta que retorne el monto total de ventas de cada producto
ordenado de mayor a menor.*/
SELECT producto.descripcion, SUM(monto*cantidad) AS monto_total
FROM factura_producto 
	INNER JOIN producto 
    INNER JOIN precio
	WHERE factura_producto.id_producto = producto.id_producto 
    AND producto.id_precio = precio.id_precio
GROUP BY producto.descripcion
ORDER BY monto_total DESC;

/*7. Realizar una consulta que retorne la cantidad total de facturas por cliente ordenado
de mayor a menor incluyendo clientes que no tengan facturas cuya cantidad total será 0.*/
SELECT apellido, COUNT(cliente.id_cliente = factura.id_cliente) AS cant_total
FROM cliente 
	LEFT JOIN factura
	ON cliente.id_cliente = factura.id_cliente
GROUP BY cliente.id_cliente
ORDER BY cant_total DESC;

/*8. Realizar una vista llamada compras que retorne código, apellido y nombre del cliente,
número y fecha de todas las facturas, código, descripción, precio unitario, cantidad y
precio total de los productos incluidos en cada factura.*/
CREATE VIEW compras_vw 
    AS SELECT cliente.id_cliente, cliente.apellido, cliente.nombre, 
		factura.numero, factura.fecha, 
        producto.codigo, producto.descripcion, 
        precio.monto, factura_producto.cantidad, 
        SUM(cantidad*monto) AS precio_total
    FROM cliente
    INNER JOIN factura 
    INNER JOIN factura_producto
    INNER JOIN producto
    INNER JOIN precio
    WHERE cliente.id_cliente = factura.id_cliente
    AND factura.id_factura = factura_producto.id_factura
    AND factura_producto.id_producto = producto.id_producto
    AND producto.id_precio = precio.id_precio
GROUP BY cliente.apellido
ORDER BY precio_total DESC;

/*9. Realizar una consulta sobre la vista compras para un cliente en particular identificado
por código.*/

SELECT * 
	FROM compras_vw
    WHERE cliente.codigo = D3;
    
/*10. ¿qué índices son necesarios para optimizar las consultas anteriores?
Para la consigna 8 seria conveniente tener los siguientes indices:
- 
*/

/*11. Se quiere consultar sobre la vista compras por el campo apellido (utilizando operador
like %), obtenga el plan de ejecución de la vista realizando una consulta para un apellido,
realice el índice que más le convenga para evitar el full scan y obtenga nuevamente el
plan de ejecución, la entrega de este ítem es la creación del índice.*/

-- Consulta usando operador like%
SELECT *
	FROM compras_vw
    WHERE apellido LIKE "L%";

-- Plan de ejecucion de la consulta anterior
EXPLAIN SELECT *
	FROM compras_vw
    WHERE apellido LIKE "L%";

-- Creacion de un indice para la coluna apellido de la tabla cliente
CREATE INDEX cliente_apellido_idx ON cliente (apellido);
