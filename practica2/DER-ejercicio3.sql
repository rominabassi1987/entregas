CREATE DATABASE tpclientes;
USE practica2;

/* 2) Realizar el DER (Diagrama Entidad Relación), se sugiere hacerlo con el MySQL
Workbrench, guardar el mismo en una carpeta “sql” dentro del proyecto. */
CREATE TABLE cuenta (
	id_cuenta INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    numero VARCHAR(256) NOT NULL,
    cliente LONG NOT NULL
/*FOREIGN KEY (cliente) REFERENCES cliente(id)*/
);

CREATE TABLE cliente (
	id_cliente INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    codigo VARCHAR(256) NOT NULL,
    apellido VARCHAR(256) NOT NULL,
    nombre VARCHAR(256) NOT NULL,
    cuenta INT NOT NULL,
    /*factura LIST */
FOREIGN KEY (cuenta) REFERENCES cuenta(id_cuenta)
);

ALTER TABLE cuenta
	ADD FOREIGN KEY (cliente) REFERENCES precio(id_precio);